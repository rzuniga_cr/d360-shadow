require File.dirname(__FILE__) + '/metadata.rb'

module Shadow
  class Engine < ::Rails::Engine
    isolate_namespace Shadow

    include Shadow::Metadata
  end
end
