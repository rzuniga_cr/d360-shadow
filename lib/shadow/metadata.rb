module Shadow
  module Metadata
    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      def descriptive_name
        'APPS.D360SHADOW'
      end

      def app_key
        'D360SHADOW'
      end

    end
  end
end
