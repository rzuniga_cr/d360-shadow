$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "shadow/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "shadow"
  s.version     = Shadow::VERSION
  s.authors     = ["Your name"]
  s.email       = ["Your email"]
  s.homepage    = ""
  s.summary     = "Summary of Shadow."
  s.description = "Description of Shadow."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.22.3"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
end
