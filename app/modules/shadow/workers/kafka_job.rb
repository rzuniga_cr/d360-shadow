require 'json'
require 'timeout'
require 'resque-retry'
module Jobs
  module KafkaJob
    class PushJob
      extend Resque::Plugins::ExponentialBackoff
      @backoff_strategy = [1, 3, 5, 10, 20]
      @retry_delay_multiplicand_min = 1.0
      @retry_delay_multiplicand_max = 1.0
      @queue = :kafka

      def self.perform(message)
        begin
          CoefKafkaClient::Event.new(message['message'], message['event'], Rails.logger).send
        rescue => e
          Resque.logger.info "Resque Job Failed: Push to Kafka queue for message #{message} failed"
          Resque.logger.info "Retry attempt: #{@retry_attempt}"

          log_error(e) if @retry_attempt == @backoff_strategy.size

          raise e
        end
      end

      def self.log_error(e)
        D360Core::D360_Logger.log_to_sentry e
        Resque.logger.info e.backtrace.join("\n")
      end
    end
  end
end
