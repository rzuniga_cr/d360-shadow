require 'aws-sdk-core'
require 'aws-sdk-sqs'
require 'json'
require 'timeout'
require 'resque-retry'
module Jobs
  module SqsJob
    class PushJob
      extend Resque::Plugins::ExponentialBackoff
      @backoff_strategy = [1, 3, 5, 10, 20]
      @retry_delay_multiplicand_min = 1.0
      @retry_delay_multiplicand_max = 1.0
      @queue = :sqs

      def self.perform(message)
        begin
          sqs_creds = ::SqsCredentials.get_credential
          Aws.config.update({
            region: sqs_creds['region'],
            credentials: Aws::Credentials.new(sqs_creds['access_key_id'],
             sqs_creds['secret_access_key'] )
          })

          user_tracking_queue = USER_TRACKING['queue']
          if user_tracking_queue.blank?
            Resque.logger.info "User Tracking not configured properly. Hence push is not possible"
          else
            Timeout.timeout(GlobalConstants::SQS_TIMEOUT_THRESHOLD) do
              sqs = Aws::SQS::Client.new
              queue_url = sqs.get_queue_url(queue_name: user_tracking_queue['name']).queue_url
              sqs.send_message({queue_url: queue_url, message_body: message.to_json})
            end
          end
        rescue => e
          Resque.logger.info "Resque Job Failed: Push to SQS queue #{user_tracking_queue} for message #{message} failed"
          Resque.logger.info "Retry attempt: #{@retry_attempt}"

          log_error(e) if @retry_attempt == @backoff_strategy.size

          raise e
        end
      end

      def self.log_error(e)
        D360Core::D360_Logger.log_to_sentry e
        Resque.logger.info e.backtrace.join("\n")
      end
    end
  end
end
