module Shadow
  module Workers
    module D360ExpressService
      class PartsLaborSalesSummaryReportJob

        @queue = 'async_report_parts_labor_sales_summary'

        attr_accessor :report
        attr_accessor :options

        FILE_NAME = 'express_service_parts_labor_sales_summary'

        def	enqueue(options)
          Resque.enqueue(self.class, options)
          return {code: 200}
        end

        def self.perform(options)
          current_program = options.delete(:current_program)
          if current_program.present? && current_program.respond_to?(:id)
            options[:program_id] = current_program.id
          end
          generate_report(options)
        end

        def generate_report(options)
          @options = ActiveSupport::HashWithIndifferentAccess.new(options)
          file_path = generate_xls_report_new
        end

        def generate_pdf_report
          if options[:new_pdf]
            generate_new_pdf_report
          else
            generate_old_pdf_report
          end
        end

        def generate_xls_report
          # using "temp_report_dir" method present in the BaseReportGenerator class
          # to define the temporary path where this report pdf will be stored
          directory_name = temp_report_dir(ExpressService20::Engine.app_key)

          # creating the required directory is not present
          FileUtils.mkdir_p(directory_name) unless File.exists?(directory_name)

          file_name = ExpressService20::EsReport::FILE_NAME
          set_reportname(file_name)
          file_path = ''

          #Generating file name based on type of report
          current_timestamp = Time.now.to_i
          from_month = Date::MONTHNAMES[options[:from_date_month].to_i][0..2].downcase
          if(options[:filter_by] == "brand")
            rollup_to_single = options[:rollup_to_single] == "1" ? 'rollup' : 'filtered'
            file_name = "#{file_name}_#{rollup_to_single}_#{from_month}_#{options[:from_date_year]}"
          elsif (options[:filter_by] == "dealership" && !options[:dealership_id].blank?)
            dealership = D360Core::Dealership.find_by_id_and_active(options[:dealership_id].to_i, true)
            entity_code = dealership.nil? ? "" : dealership.code
            rollup_to_single = options[:rollup_to_single] == "1" ? 'rollup_' : ''
            file_name = "#{file_name}_#{entity_code}_#{rollup_to_single}#{from_month}_#{options[:from_date_year]}"
          end

          # defining the full path of the file using dealership and report information
          file_path = "#{directory_name}/#{file_name}#{report_timestamp}.xls"

          file_path = ExpressService20::EsReport.generate_es_report(options, file_path)
        end

        def set_reportname(file_name)
          @reportname ||= file_name
        end

        def generate_xls_report_new
          @reportname = report.report_name
          report.generate
        end

        def generate_new_pdf_report
          @reportname = pdf_report.report_name
          pdf_report.generate
        end

        def report
          @report ||= options[:plss_v2] ?
            ExpressService20::PLSS::V2::Report.new(options) : ExpressService20::PLSS::Report.new(options)
        end

        def pdf_report
          @pdf_report ||= options[:plss_v2] ?
            ExpressService20::PLSS::V2::PDFReport.new(options) : ExpressService20::PLSS::PDFReport.new(options)
        end

      end
    end
  end
end
