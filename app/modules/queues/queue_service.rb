module Queues
  class QueueService
    def enqueue(job_class, options)
      Resque.enqueue(job_class, options)
    end
  end
end
